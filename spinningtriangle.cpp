#include <QQuickWindow>
#include <QOpenGLContext>

#include "spinningtriangle.h"

SpinningTriangle::SpinningTriangle()
{
    stRenderer = 0;
    connect(this, &QQuickItem::windowChanged, this, &SpinningTriangle::handleWindowChanged);
}


void SpinningTriangle::handleWindowChanged(QQuickWindow *win)
{
    if (win)
    {
        connect(win, &QQuickWindow::beforeSynchronizing, this, &SpinningTriangle::sync, Qt::DirectConnection);              //openGL context should be bound before the scene
                                                                                                                            //graph synchronization with QML
        connect(win, &QQuickWindow::sceneGraphInvalidated, this, &SpinningTriangle::cleanup, Qt::DirectConnection);

        win->setClearBeforeRendering(false);                                                                                //openGL renders before scene graph, so need to turn off
                                                                                                                            //frame buffer cleaning before sxene graph rendering
    }
}


void SpinningTriangle::sync()
{
    if (!stRenderer)
    {
        stRenderer = new SpinningTriangleRenderer();
        connect(window(), &QQuickWindow::beforeRendering, stRenderer, &SpinningTriangleRenderer::paint, Qt::DirectConnection);      //render openGL before the scene graph
    }
    stRenderer->setViewportSize(window()->size() * window()->devicePixelRatio());
    stRenderer->setWindow(window());

}


void SpinningTriangle::cleanup()
{
    if (stRenderer)
    {
        delete stRenderer;
        stRenderer = 0;
    }
}


SpinningTriangleRenderer::SpinningTriangleRenderer()
{
    stProgram = 0;
    currentSpinAngle = 0;
    currentColor = 0;
}


SpinningTriangleRenderer::~SpinningTriangleRenderer()
{
    delete stProgram;
}


void SpinningTriangleRenderer::paint()
{
    if (!stProgram)
    {
        initializeOpenGLFunctions();

        stProgram = new QOpenGLShaderProgram();
        stProgram->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex,                                                //GLSL vertex shaders
                                                    "attribute vec4 vertices;"                                            //get attributes (initial vertices coordinates)
                                                    "uniform mat4 mvp;"                                                   //get uniform matrix (model view projection matrix)
                                                    "void main()"
                                                    "{"
                                                    "    gl_Position = mvp * vertices;"                                   //set vertices coordinates
                                                    "}");
        stProgram->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment,                                              //GLSL fragment shader
                                                    "uniform vec4 color;"                                                 //get uniform colot vector
                                                    "void main()"
                                                    "{"
                                                    "    gl_FragColor = color;"                                           //set triangle color
                                                    "}");

        stProgram->link();                                                                                                //link shaders together
    }

    stProgram->bind();                                                                                                    //bind program to the openGL context and make it active
    stProgram->enableAttributeArray("vertices");                                                                          //"vertices" from shader now can be set with array

    float verticesCoordinates[] =                                                                                         //triangle initial vertices coordinates array
    {
        -0.5, -0.5,
        0, 0.5,
        0.5, -0.5
    };    
    stProgram->setAttributeArray("vertices", GL_FLOAT, verticesCoordinates, 2);                                           //"vertices" set with triangle vertices coordinates array


    QMatrix4x4 mvp;                                                                                                        //model view projection matrix
    mvp.perspective(60.0, 1.0, 0.1, 100.0);                                                                                //set perspective
    mvp.translate(0, 0, -2);                                                                                               //set triangle initial coordinates in world coordinates system
    mvp.rotate(currentSpinAngle, 0, 1, 0);                                                                                 //set rotation during one iteration

    stProgram->setUniformValue("mvp", mvp);                                                                                //"mvp" from shader set with mvp matrix

    currentSpinAngle += 1;                                                                                                 //update rotation angle
    currentSpinAngle %= 360;

    float colors[] = {0.0, 0.0, 0.0, 1.0};                                                                                 //triangle colors array
    if (currentColor < 256)                                                                                                //color change algorithm
    {
        colors[0] = 255 - currentColor;
        colors[1] = currentColor;
        colors[2] = 0;
    }
    else
    {
        if (currentColor < 511)
         {
             colors[0] = 0;
             colors[1] = 255 - (1 + currentColor % 256);
             colors[2] = 1 + currentColor % 256;
         }
         else
         {
            colors[0] = (2 + currentColor) % 256;
            colors[1] = 0;
            colors[2] = 255 - ((2 + currentColor) % 256);;
         }
    }

    colors[0] /= 255.0;
    colors[1] /= 255.0;
    colors[2] /= 255.0;
    colors[4] = 1.0;
    stProgram->setUniformValueArray("color", colors, 1, 4);                                                               //"color" from shader set with colors array

    currentColor += 1;
    currentColor %= 766;

    glViewport(0, 0, stViewportSize.width(), stViewportSize.height());                                                    //set openGL viewport size as window size

    glClearColor(0, 0, 0, 1);                                                                                             //make the background color black after cleaning clor buffer
    glClear(GL_COLOR_BUFFER_BIT);                                                                                         //clear color buffer

    glDrawArrays(GL_TRIANGLES, 0, 3);                                                                                     //draw triangle using triangle primitive

    stProgram->disableAttributeArray(0);                                                                                  //attribute arrays shoul be disabled before
    stProgram->release();                                                                                                 //releasing current active program from openGL context

    stWindow->resetOpenGLState();                                                                                         //reset openGL context
    stWindow->update();                                                                                                   //update QQuickItem window
}

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "spinningtriangle.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<SpinningTriangle>("SpinningTriangle", 1, 0, "SpinningTriangle");        //registration of SpinningTriangle class in QML

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

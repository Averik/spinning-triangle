#ifndef SPINNINGTRIANGLE_H
#define SPINNINGTRIANGLE_H

#include <QQuickItem>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>


class SpinningTriangleRenderer : public QObject, protected QOpenGLFunctions             //this class needed because rendering can happen in different thread from gui
{
    Q_OBJECT

public:
    SpinningTriangleRenderer();
    ~SpinningTriangleRenderer();

    void setViewportSize(const QSize &size) { stViewportSize = size; }                  //for setting viewport size from window size (available in QQuick Item)
    void setWindow(QQuickWindow *window) { stWindow = window; }                         //pointer to QQuickwindow will be used to reset openGL state

private:
    QSize stViewportSize;
    QQuickWindow *stWindow;
    QOpenGLShaderProgram *stProgram;
    int currentSpinAngle;
    int currentColor;

public slots:
    void paint();
};


class SpinningTriangle : public QQuickItem                                              //this class provides working with QML
{
    Q_OBJECT

public:
    SpinningTriangle();

private:
    SpinningTriangleRenderer *stRenderer;

public slots:
    void sync();                                                                        //synchronization with gui
    void cleanup();                                                                     //cleaning resourses after scene graph invalidation

private slots:
    void handleWindowChanged(QQuickWindow *win);                                        //provide reaction on QQuickItem window changes
};


#endif // SPINNINGTRIANGLE_H

import QtQuick 2.4
import QtQuick.Window 2.2
import SpinningTriangle 1.0

Window {
    visible: true
    width: 512
    height: 512
    title: ("Spinning triangle")
    minimumHeight: 512
    maximumHeight: 512
    minimumWidth: 512
    maximumWidth: 512

    Item
    {
        SpinningTriangle
        {

        }
    }

    /*MainForm {
        anchors.fill: parent
    }*/
}
